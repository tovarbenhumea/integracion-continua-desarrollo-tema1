# Bitbucket repository - Equipo 1 - Integración Continua (I): desarrollo del producto #

Este es el repositorio de Bitbucket creado por los integrantes del **Equipo 1** para la materia **Integración continua** del master en **DevOps & Cloud Computing**

*Integrantes:*

- Yaneth Alvarado
- Jessica Menesses
- Pablo Araújo
- Ariel Flores
- David Tovar 


---


## Proceso de configuración de entorno (Linux) ##

1. Descargar e instalar la última versión de estable de NodeJS
   
   ```
   sudo apt-get update
   sudo apt-get install nodejs npm -y
   ```
   
   ![Descargar NodeJS](images/descargar_node_linux.png)
   

2. Descargar e instalar el IDE Visual Studio Code

   ```
   wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
   sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
   sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
   sudo apt update
   sudo apt install code -y
   ```
   
   ![Descargar VSCode](images/descargar_vscode_linux.png)


3. Descargar e instalar GIT

   ```
   sudo apt-get install git -y
   ```
   
   ![Instalar git](images/instalar_git.png)


4. Descargar el proyecto utilizando el comando `git clone` del presente repositorio
   
   ```
   git clone https://<Bitbucket user>@bitbucket.org/areaujo/integracion-continua-desarrollo-tema1.git
   ```
   
   ![Git clone](images/git_clone.png)
   ![VSCode open](images/vscode_abrir_clone.png)
   

5. Ejecutar el comando `npm install` dentro de la carpeta del proyecto para instalar las dependencias del proyecto

   ```
   npm install
   ```
   
   ![NPM install](images/npm_install.png)


6. Ejecutar el comando `npm start` dentro de la carpeta del proyecto para iniciar un server y poder verificar la aplicación, **localhost:3000**.

   ```
   npm start
   ```
   
   ![NPM start](images/npm_start.png)


---

:abc:
